package pl.kkwm.photomanager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;


public class ViewPhotoActivity extends ActionBarActivity {
    private LinearLayout layout;
    private File[] files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);

        //odwo�a� si� do firstApp katalogu w dcim
        //wylistowa� pliki
        //zak�adami, �e wszystkie to .jpg
        //tworzy� dynamicznie imageview
        //dodawa� do imageview te obrazki

        File dcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photos = new File(dcim.getAbsolutePath() + "/firstApp");
        layout = (LinearLayout) findViewById(R.id.layout);

        files=photos.listFiles();
        for (int i = 0; i <files.length ; i++) {
            ImageView a = new ImageView(ViewPhotoActivity.this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,400);
            params.setMargins(10, 5, 10, 5);
            a.setLayoutParams(params);
            Bitmap myBitmap = BitmapFactory.decodeFile(files[i].getAbsolutePath());
            a.setScaleType(ImageView.ScaleType.CENTER_CROP);
            a.setId(100 + i);
            layout.addView(a);
            a.setImageBitmap(myBitmap);

            Log.d("MYAPP", files[i].getAbsolutePath());

            a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(ViewPhotoActivity.this,ThisPhotoActivity.class);
                    intent.putExtra("path",files[v.getId()-100].getAbsolutePath());
                    startActivity(intent);
                }
            });

        }


    }
}
