package pl.kkwm.photomanager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


public class ThisPhotoActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_this_photo);

        String sciezka=getIntent().getExtras().getString("path");
        Log.d("MYAPP", "Open photo " + sciezka);
        ImageView a = (ImageView) findViewById(R.id.photo);
        Bitmap myBitmap = BitmapFactory.decodeFile(sciezka);
        a.setImageBitmap(myBitmap);
    }
}
