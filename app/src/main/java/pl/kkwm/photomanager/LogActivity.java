package pl.kkwm.photomanager;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LogActivity extends ActionBarActivity {
    private EditText loguj;
    private EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        loguj = (EditText) findViewById(R.id.logData);
        pass = (EditText) findViewById(R.id.passData);
        Button login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loguj.getText().toString().equals("maciek") && pass.getText().toString().equals("123")){
                    Intent intent = new Intent(LogActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(LogActivity.this,"nie zaloguje",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}
