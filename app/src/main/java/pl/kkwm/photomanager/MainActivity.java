package pl.kkwm.photomanager;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //viewPhoto
        Button viewPhoto=(Button) findViewById(R.id.viewPhoto);
        viewPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this,"Kliknąłem se",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(MainActivity.this,ViewPhotoActivity.class);
                //Intent inten1=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(intent);
                //finish();
            }
        });

        Button takePhoto=(Button) findViewById(R.id.takePhoto);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String datetime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File dcim=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                File photo= new File(dcim.getAbsolutePath()+"/firstApp");

                if(!photo.exists()) {
                    photo.mkdir();
                }

                File thisPhoto=new File(photo.getAbsolutePath()+"/"+datetime+".jpg");

                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(thisPhoto)); // set the image file name

                startActivityForResult(intent, 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                //Toast.makeText(this, "Image saved to:\n" + data.getData(), Toast.LENGTH_LONG).show();
                Toast.makeText(this, "Image saved", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

    }
}
